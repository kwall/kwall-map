**Moorpark College**\
NE corner: 34.30579, -118.82606\
SE corner: 34.29375, -118.82606\
SW corner: 34.29375, -118.84662\
NW corner: 34.30582, -118.84666

**Oxnard College**\
NE corner: 34.17144, -119.14839\
SE corner: 34.15832, -119.14843\
SW corner: 34.15834, -119.16594\
NW corner: 34.17143, -119.16591

**Ventura College**\
NE corner: 34.28395, -119.21637\
SE corner: 34.27005, -119.21639\
SW corner: 34.27005, -119.24491\
NW corner: 34.28395, -119.24491
