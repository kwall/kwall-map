<?php

namespace Drupal\kwall_map\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class KWALLMapForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'kwall_map.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kwall_map_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('kwall_map.settings');

    $form['overlay_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Overlay path'),
      '#description' => $this->t('Set relative or absolute path to custom overlay. Tokens supported. Empty for default.'),
      '#default_value' => $config->get('overlay_path'),
    ];

    $form['sw_lat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Southwest Latitude'),
      '#description' => $this->t('Set the Southwest Latitude coordinates. Empty for default.'),
      '#default_value' => $config->get('sw_lat'),
    ];
    $form['sw_lon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Southwest Longitude'),
      '#description' => $this->t('Set the Southwest Longitude coordinates. Empty for default.'),
      '#default_value' => $config->get('sw_lon'),
    ];
    $form['ne_lat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Northeast Latitude'),
      '#description' => $this->t('Set the Northeast Latitude coordinates. Empty for default.'),
      '#default_value' => $config->get('ne_lat'),
    ];
    $form['ne_lon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Northeast Longitude'),
      '#description' => $this->t('Set the Northeast Longitude coordinates. Empty for default.'),
      '#default_value' => $config->get('ne_lon'),
    ];

    $form['style'] = [
      '#type' => 'textarea',
      '#title' => $this->t('JSON styles'),
      '#description' => $this->t('A JSON encoded styles array to customize the presentation of the Google Map.'),
      '#default_value' => $config->get('style'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('kwall_map.settings')
      ->set('overlay_path', $form_state->getValue('overlay_path'))
      ->set('sw_lat', $form_state->getValue('sw_lat'))
      ->set('sw_lon', $form_state->getValue('sw_lon'))
      ->set('ne_lat', $form_state->getValue('ne_lat'))
      ->set('ne_lon', $form_state->getValue('ne_lon'))
      ->set('style', $form_state->getValue('style'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}